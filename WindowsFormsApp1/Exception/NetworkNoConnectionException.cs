﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class NetworkNoConnectionException : Exception
{
	public NetworkNoConnectionException(string message) : base("No internet connection: " + message)
	{
	}
}
