﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using HtmlAgilityPack;
using LibVLCSharp.Shared;
using musashiro.Utils;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace musashiro
{  
	public partial class musashiro : Form
	{
		public LibVLC _libVLC;
		public MediaPlayer _mp;
		private bool isPlaying = true;
		private int volume = 40;
		private bool dragging = false;
		private Point dragCursorPoint;
		private Point dragFormPoint;
		private const string url = "https://musashiro.ir:8443/basic-radio.ogg";
		public musashiro()
		{
			InitializeComponent();
			LibVLCSharp.Shared.Core.Initialize();
			try
			{
				ConectionTester.CheckForInternetConnection();
				_libVLC = new LibVLC();
				_mp = new MediaPlayer(_libVLC);
				_mp.Play(new Media(_libVLC, url, FromType.FromLocation));
				_mp.Volume = volume;
				playing.Text = crawler().Result[5].InnerText.Replace("\n", "").Replace("\t", "").Trim();
				this.textBox1.Text = "Volume: " + volume;
			}
			catch (NetworkNoConnectionException e)
			{
				MessageBox.Show(e.Message, "Network Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				this.Close();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message, "Unknown", MessageBoxButtons.OK, MessageBoxIcon.Stop);
				this.Close();
			}
			
			
		}

		public static async Task<List<HtmlNode>> crawler()
		{
			var httpClient = new HttpClient();
			string data;
			using (WebClient web1 = new WebClient())
			{
				data = web1.DownloadString("https://musashiro.ir:8443");
			}

			Console.WriteLine(data);
			HtmlDocument doc = new HtmlDocument();
			doc.LoadHtml(data);
			var res = doc.DocumentNode.Descendants("td").Where(node => node.GetAttributeValue("class", "").Equals("streamstats"))
				.ToList();
			return res;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			_mp.Pause();
			isPlaying = !isPlaying;
			if (!isPlaying)
				button1.Text = "PAUSE";
			else
				button1.Text = "Play";
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			
		}

		private void button2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
			
		}

		private void button3_Click(object sender, EventArgs e)
		{
			if(volume > 0)
				volume -= 10;
			_mp.Volume = volume;
			this.textBox1.Text = "Volume: " + volume;

		}

		private void button5_Click(object sender, EventArgs e)
		{
			if (volume < 100)
				volume += 10;
			_mp.Volume = volume;
			this.textBox1.Text = "Volume: " + volume;

		}
		private void panel1_MouseDown(object sender, MouseEventArgs e)
		{
			dragging = true;
			dragCursorPoint = Cursor.Position;
			dragFormPoint = this.Location;
		}

		private void panel1_MouseMove(object sender, MouseEventArgs e)
		{
			if (dragging)
			{
				Point dif = Point.Subtract(Cursor.Position, new Size(dragCursorPoint));
				this.Location = Point.Add(dragFormPoint, new Size(dif));
			}
		}

		private void panel1_MouseUp(object sender, MouseEventArgs e)
		{
			dragging = false;
		}
		private void Form1_Load(object sender, EventArgs e)
		{
			Timer timer = new Timer();
			timer.Interval = (5 * 1000); // 5 secs
			timer.Tick += new EventHandler(tick);
			timer.Start();
		}
		private void tick(object sender, EventArgs e)
		{
			try
			{
				ConectionTester.CheckForInternetConnection();
			}
			catch (NetworkNoConnectionException ex)
			{
				playing.Text = ex.Message;
				button1.Hide(); 
				return;
			}
			if(!_mp.IsPlaying)
				_mp.Play(new Media(_libVLC, url, FromType.FromLocation));
			button1.Show();
			playing.Text = crawler().Result[5].InnerText.Replace("\n", "").Replace("\t", "").Trim();
		}
	}
}
